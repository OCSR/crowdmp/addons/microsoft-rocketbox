# Microsoft version of Rocketbox
This is the rocketbox shared by microsoft. You can find their git "[here](https://github.com/microsoft/Microsoft-Rocketbox)".

## Import
The models have been imported in unity as humanoid to works with our animations.
Importing them again can break the animation and texture. (if the rocketbox are missing texture a fail import might have occured) 
In case you need to do a new import, here is a list of things to look out for:
1. There is a script named "FixRocketboxMaxImport.cs" that should be reactivated for the import by defining the Scripting Symbols "MICROSOFTRB" in the project setting. This script reorganizes the bones hierarchy correctly and fix the texture import.
2. The models might be flying after an import. You can fix it by configuring each avatar, reseting the pose and then enforcing T-pose.
